#!/usr/bin/env python3

import sys
import pandas as pd
import pylab as P
import matplotlib.pyplot as plt
from sklearn.ensemble import ExtraTreesClassifier as Classifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.tree import ExtraTreeClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import SGDClassifier
from sklearn.linear_model import Perceptron

def scatter_survived(x):
    train.plot(kind='scatter', x='Survived', y=x)
    P.show()

def scatter(x, y):
    ax = train[train.Survived == 1].plot(kind='scatter', x=x, y=y, label='Survived', color='Red')
    train[train.Survived == 0].plot(kind='scatter', x=x, y=y, label='Not Survived', ax=ax)
    P.show()

def hexbin(x, y, train):
    train.plot(kind='hexbin', x=x, y=y, C='Survived', gridsize=10)
    P.show()

def classifier():
    return RandomForestClassifier(n_estimators=285, max_depth=14, min_samples_split=3, random_state=0)

def map_status(x) :
    if 'Mrs.' in x: return 0
    if 'Miss.' in x: return 0
    if 'Major.' in x: return 4
    if 'Master.' in x: return 5
    if 'Mr.' in x: return 0
    return 3

def normalize(*dataframes):
    for dataframe in dataframes:
        dataframe = dataframe / (dataframe.max() - dataframe.min())

#hexbin('Age', 'Gender', train[train.Gender==1])
def group_age_w(x):
    if x >= 56: return 90
    if x >= 32 and x <= 43: return 84
    if x >= 43: return 78
    if x <= 7: return 76
    if x >= 19: return 74
    return 0

def group_age_m(x):
    if x <= 9: return 60
    if x >= 70: return 48
    if x <= 55: return 30
    return 12

def prepare_data(train_origin, is_train_set=True):
    if is_train_set:
        train = train_origin
        train = train[pd.notnull(train.Embarked)][train.columns]
        train = train[pd.notnull(train.Fare)][train.columns]
        train = train[pd.notnull(train.Age)][train.columns]
        train = train[train.Fare <= 300]
        print(len(train), len(train_origin))
    else:
        train = train_origin
    train['Gender'] = train.Sex.map( lambda x: 1 if x == 'female' else 0 )
    mean_age = train.Age.mean()
    train['Age'] = train.Age.map( lambda x: x if pd.notnull(x) else mean_age )
    train['AgeGroupW'] = train.Age.map( lambda x: group_age_w(x) )
    train['AgeGroupM'] = train.Age.map( lambda x: group_age_m(x) )
    mean_fare = train.Fare.mean()
    train['Fare'] = train.Fare.map( lambda x: x if pd.notnull(x) else mean_fare )
    destinations = {'C': 3, 'Q': 1, 'S': 2}
    train['Destination'] = train.Embarked.map( lambda x: destinations[x] if not pd.isnull(x) else 1 )
    train['DestinationPclass'] = train.Destination / train.Pclass
    train['Family'] = train.SibSp + train.Parch
    train['GenderAgeW'] = train.Gender * train.AgeGroupW
    train['GenderAgeM'] = (train.Gender - 1) * train.AgeGroupM
    train['Status'] = train.Name.map(lambda x: map_status(x))
    train = train.drop(['Sex', 'Embarked', 'Name', 'Ticket', 'Cabin', 'PassengerId'], axis=1)
    return train

def select(train, fields):
    result = train
    columns = result.columns
    for field in columns:
        if field not in fields:
            result = result.drop([field], axis=1)
    return result

def predict(train, test):
    forest = classifier()
    Xtrain = train.drop(['Survived'], axis=1)
    forest = forest.fit(Xtrain, train.Survived)
    survive_predicted = forest.predict(test)
    test['Survived'] = survive_predicted
    return test

train_origin = pd.read_csv('train.csv')
print(train_origin.columns)
train = prepare_data(train_origin)
fields = ['Survived', 'GenderAgeM', 'GenderAgeW', 'DestinationPclass', 'Fare', 'Status', 'Family']

row_count = len(train) * 2 // 3
trainee = select(train[:row_count], fields)
test = select(train[row_count:].drop(['Survived'], axis=1), fields)
test = predict(trainee, test)
train.Survived.corr(test.Survived)
print('correlation1.1 with train set:', train.Survived.corr(test.Survived))
row_count = len(train) * 1 // 3
trainee = select(train[row_count:], fields)
test = select(train[:row_count].drop(['Survived'], axis=1), fields)
test = predict(trainee, test)
train.Survived.corr(test.Survived)
print('correlation1.2 with train set:', train.Survived.corr(test.Survived))
row_count = len(train)
trainee = select(train, fields)
test = select(train.drop(['Survived'], axis=1), fields)
test = predict(trainee, test)
train.Survived.corr(test.Survived)
print('correlation2 with train set:', train.Survived.corr(test.Survived))


train = select(train, fields)
test_origin = pd.read_csv('test.csv')
test = prepare_data(test_origin, False)
test = select(test, fields)
test = predict(train, test)
test['PassengerId'] = test_origin['PassengerId']
filename = '_'.join(fields) + '.csv'
print('filename: ', filename)
test.to_csv(filename, columns=['PassengerId', 'Survived'], index=False)
